//
// Created by alexb on 20-Oct-19.
//

#include <iostream>
#include <chrono>
#include "ProjectEuler/MainEuler.h"

using namespace std;

int main(int argc, char *argv[]) {
    MainEuler mainEuler;

    auto start = chrono::high_resolution_clock::now();

    if (argc == 2) {
        mainEuler.solve(atoi(argv[1]));
    } else {
        mainEuler.solve(7);
    }

    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::duration<float>>(stop - start);
    cout << endl << "Time: " << duration.count() << " s" << endl;

//    mainEuler.print_problems_names();
}