#include "../Problem.h"

#include <iostream>
#include <cmath>

using namespace std;

class Problem10: public Problem {
    bool check_if_prime(long long number) {
        long long divisor;

        if (number != 2 && number % 2 == 0)
            return false;

        for (divisor = 3; divisor <= sqrt(number); divisor+=2)
            if (number % divisor == 0)
                return false;

        return true;
    }

    long long sum_of_primes(long limit) {
        long long sum = 2+3;
        for (long i = 6; i < limit; i+=6) {
            if (check_if_prime(i-1)) {
                sum+=(i-1);
            }
            if(check_if_prime(i+1)) {
                sum+=(i+1);
            }
        }

        return sum;
    }
public:
    void solve() override {
        cout << sum_of_primes(2000000);
    }

    std::string get_problem_name() override {
        return std::string("Summation of primes");
    }
};
