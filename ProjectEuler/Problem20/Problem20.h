#include "../Problem.h"

#include <iostream>
#include <vector>

using namespace std;

class Problem20: public Problem {
    long factorial_digits_sum(int factorial) {
        vector<int> digits;
        digits.push_back(1);

    //    Multiply the factorial by each number till n!
        for (int n = 1; n <= factorial; ++n) {
            int value = 0;
            for (int digit = static_cast<int>(digits.size() - 1); digit >= 0; --digit) {
                value += n * digits[digit];
                digits[digit] = value % 10;
                value /= 10;
            }
            while (value > 0) {
                digits.insert(digits.begin(), value % 10);
                value /= 10;
            }
    //        We can drop the final 0-s from the result
            while(*(digits.end()-1) == 0) {
                digits.pop_back();
            }
        }

        long sum = 0;
        for (auto digit : digits) {
            sum += digit;
        }

        return sum;
    }
public:
    void solve() override {
        cout << factorial_digits_sum(100);
    }

    std::string get_problem_name() override {
        return std::string("Factorial digit sum");
    }
};
