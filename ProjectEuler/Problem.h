//
// Created by alexb on 20-Oct-19.
//

#ifndef PROGRAMMINGCHALLENGESC_PROBLEM_H
#define PROGRAMMINGCHALLENGESC_PROBLEM_H

#include <string>

class Problem {
public:
//    Solves the problem and prints the solution, followed by an endline
    virtual void solve() = 0;
//    Return the problem actual name
    virtual std::string get_problem_name() = 0;
};


#endif //PROGRAMMINGCHALLENGESC_PROBLEM_H
