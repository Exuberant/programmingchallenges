//
// Created by alexb on 20-Oct-19.
//

#ifndef PROGRAMMINGCHALLENGESC_PROBLEM2_H
#define PROGRAMMINGCHALLENGESC_PROBLEM2_H

#include "../Problem.h"

#include <iostream>
using namespace std;

class Problem2: public Problem {
public:
    void solve() override {
        int fib1 = 1, fib2 = 2;
        long long sum = 0;

        while (fib2 < 4000000) {
            if (fib2 % 2 == 0)
                sum += fib2;
            fib2 = fib1 + fib2;
            fib1 = fib2 - fib1;
        }

        cout << sum;
    }

    std::string get_problem_name() override {
        return std::string("Even Fibonacci numbers");
    }
};


#endif //PROGRAMMINGCHALLENGESC_PROBLEM2_H
