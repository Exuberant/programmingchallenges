//
// Created by alexb on 26-Oct-19.
//

#ifndef PROGRAMMINGCHALLENGESC_PROBLEM5_H
#define PROGRAMMINGCHALLENGESC_PROBLEM5_H

#include "../Problem.h"

#include <iostream>

using namespace std;

class Problem5: public Problem {
    long long gcd(long long a, long long b) {
        if (b == 0) return a;
        return gcd(b, a%b);
    }
public:
    void solve() override {
        long long product = 1;
        for (int p = 2; p <= 20; p++) {
            product *= p / gcd(product, p);
        }
        cout << product;
    }

    std::string get_problem_name() override {
        return std::string("Smallest multiple");
    }
};


#endif //PROGRAMMINGCHALLENGESC_PROBLEM5_H
