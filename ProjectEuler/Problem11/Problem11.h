#include "../Problem.h"

#include <iostream>

using namespace std;

class Problem11: public Problem {
public:
    void solve() override {

    }

    std::string get_problem_name() override {
        return std::string("Largest product in a grid");
    }
};
