#include "../Problem.h"

#include <iostream>
#include <map>

using namespace std;

class Problem21: public Problem {
    long long sum_of_proper_divisors(long number, long limit) {
        long long sum = 0;

        for (long divisor = 1; divisor <= number/2; ++divisor) {
            if (number % divisor == 0) {
                sum += divisor;
            }
            if (sum > limit) {
                return 0;
            }
        }

        return sum;
    }

    long long sum_of_amicable_numbers(long limit) {
        map<long, long long> divisor_sums;
        divisor_sums[0] = 0;

        for (long number = 1; number <= limit; ++number) {
            divisor_sums[number] = sum_of_proper_divisors(number, limit);
        }

        long long sum = 0;
        for (long number = 1; number <= limit; ++number) {
            long a = number;
            long long b = divisor_sums[number];
            if (a != b && a == divisor_sums[b]) {
                sum += number;
            }
        }
        return sum;
    }
public:
    void solve() override {
        cout << sum_of_amicable_numbers(10000);
    }

    std::string get_problem_name() override {
        return std::string("Amicable numbers");
    }
};
