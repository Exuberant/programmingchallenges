#include "../Problem.h"

#include <iostream>
#include <map>

using namespace std;

class Problem12: public Problem {
    long long number_of_divisors(long long n) {
        map<long long, int> prime_divisors;

        for (long long divisor = 2; n > 1; ++divisor) {
            if (n % divisor == 0) {
                auto it = prime_divisors.find(divisor);
                if (it != prime_divisors.end()) {
                    it->second++;
                } else {
                    prime_divisors.insert(pair<long long, int>(divisor, 1));
                }
                n /= divisor;
                divisor--;
            }
        }

        long long product = 1;
        for (auto &prime_divisor : prime_divisors) {
            product *= (prime_divisor.second+1);
        }

        return product;
    }

    // Return the smallest number which has minimum 'min_divs' divisors
    long long minimum_divisors(int min_divs) {
        long long triangle_number = 1;
        for (long long n = 2; true; ++n) {
            triangle_number += n;
            if (number_of_divisors(triangle_number) > min_divs) {
                return triangle_number;
            }
        }
    }
public:
    void solve() override {
        cout << minimum_divisors(500);
    }

    std::string get_problem_name() override {
        return std::string("Highly divisible triangular number");
    }
};
