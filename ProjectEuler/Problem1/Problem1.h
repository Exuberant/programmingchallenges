//
// Created by alexb on 20-Oct-19.
//

#ifndef PROGRAMMINGCHALLENGESC_PROBLEM1_H
#define PROGRAMMINGCHALLENGESC_PROBLEM1_H

#include "../Problem.h"

#include <iostream>
using namespace std;

class Problem1: public Problem {
public:
    void solve() override {
        int n1 = 3, n2 = 5;
        int sum = 0;

        for (int i = 1; i < 1000; i++)
            if (i % n1 == 0 || i % n2 == 0)
                sum += i;

        cout << sum << endl;
    }

    string get_problem_name() override {
        return string("Multiples of 3 and 5");
    }
};


#endif //PROGRAMMINGCHALLENGESC_PROBLEM1_H
