#include "../Problem.h"

#include <iostream>
#include <map>
#include <cmath>

using namespace std;

class Problem35: public Problem {
    map<int, int> allowed_digits;
    void define_allowed_digits() {
        allowed_digits[0] = 1;
        allowed_digits[1] = 3;
        allowed_digits[3] = 7;
        allowed_digits[7] = 9;
        allowed_digits[9] = 1;
    }

    bool check_if_prime(long long number) {
        for (long divisor = 3; divisor <= sqrt(number); divisor += 2) {
            if (number % divisor == 0) {
                return false;
            }
        }

        return true;
    }

    long long array_to_number(const int *digits, int shift) {
        long long number = 0;
        for (int digit = shift; digits[digit] != 0; ++digit) {
            number = number * 10 + digits[digit];
        }
        for (int digit = 0; digit < shift; ++digit) {
            number = number * 10 + digits[digit];
        }
        return number;
    }

    bool check_circular_prime(const int *digits) {
        int num_len;
        for (num_len = 0; digits[num_len] != 0; ++num_len);
        for (int shift = 0; shift < num_len; ++shift) {
            if (! check_if_prime(array_to_number(digits, shift))) {
                return false;
            }
        }
        return true;
    }

    void next_number(int *digits) {
        bool ok = true;
        for (int i = 0; i < 15 && ok; i++) {
            int digit = digits[i];
            ok = false;
            if (digit == 9) {
                ok = true;
            }
            digits[i] = allowed_digits[digit];
        }
    }
public:
    void solve() override {
        define_allowed_digits();
        int limit = 1000000;
        int digits[20] = {0};
        digits[0] = 9;

    //    there are 4 prime numbers formed with only one digit
        long numbers = 4;
        while (true) {
            next_number(digits);
            if (array_to_number(digits, 0) >= limit) {
                break;
            }
            if (check_circular_prime(digits)) {
                numbers++;
            }
        }

        cout << numbers;
    }

    std::string get_problem_name() override {
        return std::string("Circular primes");
    }
};
