#include "../Problem.h"

#include <iostream>
#include <vector>

using namespace std;

class Problem15: public Problem {
    long long lattice_paths(int lattice_size) {
        vector<vector<long long>> lattice_matrix;
        for (int i = 0; i <= lattice_size; i++) {
            vector<long long> v(lattice_size+1, 0);
            lattice_matrix.push_back(v);
        }

    //    1-border for 2 paths
        for (int i = 0; i <= lattice_size; ++i) {
            lattice_matrix[0][i] = 1;
            lattice_matrix[i][0] = 1;
        }

        for (int i = 1; i <= lattice_size; ++i) {
            for (int j = 1; j <= lattice_size; ++j) {
                lattice_matrix[i][j] = lattice_matrix[i-1][j] + lattice_matrix[i][j-1];
            }
        }

        return lattice_matrix[lattice_size][lattice_size];
    }
public:
    void solve() override {
        cout << lattice_paths(20);
    }

    std::string get_problem_name() override {
        return std::string("Lattice paths");
    }
};
