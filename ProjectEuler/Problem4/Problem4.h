//
// Created by alexb on 26-Oct-19.
//

#ifndef PROGRAMMINGCHALLENGESC_PROBLEM4_H
#define PROGRAMMINGCHALLENGESC_PROBLEM4_H

#include "../Problem.h"

#include <iostream>
#include <cstring>

using namespace std;

class Problem4: public Problem {
    bool check_if_palindrome(long long n) {
        string number = to_string(n);
        for (int i = 0; i < number.size() / 2; ++i) {
            if (number[i] != number[number.size() - i - 1]) {
                return false;
            }
        }
        return true;
    }
public:
    void solve() override {
        long long res = 0;
        for (int p = 999 ; p >= 100; --p) {
            for (int q = 999; q >= p; --q) {
                if (check_if_palindrome(p*q)) {
                    if (res < p*q) {
                        res = p*q;
                    }
                }
            }
        }

        cout << res;
    }

    std::string get_problem_name() override {
        return std::string("Largest palindrome product");
    }
};

#endif //PROGRAMMINGCHALLENGESC_PROBLEM4_H
