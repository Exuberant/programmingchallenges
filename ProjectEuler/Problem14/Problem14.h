#include "../Problem.h"

#include <iostream>
#include <map>

using namespace std;

class Problem14: public Problem {
    map<long long, long> collatz;
    long collatz_chain_length(long long n) {
        if (collatz.find(n) == collatz.end()) {
            if (n % 2 == 0) {
                collatz[n] = 1 + collatz_chain_length(n/2);
            } else {
                collatz[n] = 1 + collatz_chain_length(3*n+1);
            }
        }

        return collatz[n];
    }

    // Find the longest collatz chain starting with a number bellow a limit
    long find_longest_collatz_chain(long limit) {
        collatz[1] = 1;
        long max_chain = 1;
        long max_num = 1;

        for (long num = 2; num <= limit; ++num) {
            long chain = collatz_chain_length(num);
            if (max_chain < chain) {
                max_chain = chain;
                max_num = num;
            }
        }

        return max_num;
    }
public:
    void solve() override {
        cout << find_longest_collatz_chain(1000000);
    }

    std::string get_problem_name() override {
        return std::string("Longest Collatz sequence");
    }
};
