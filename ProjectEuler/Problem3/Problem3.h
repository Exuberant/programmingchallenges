//
// Created by alexb on 20-Oct-19.
//

#ifndef PROGRAMMINGCHALLENGESC_PROBLEM3_H
#define PROGRAMMINGCHALLENGESC_PROBLEM3_H

#include "../Problem.h"

#include <iostream>
#include <cmath>

using namespace std;

class Problem3: public Problem {
    bool check_if_prime(long long number) {
        long long divisor;

        if (number != 2 && number % 2 == 0)
            return false;

        for (divisor = 3; divisor <= sqrt(number); divisor+=2)
            if (number % divisor == 0)
                return false;

        return true;
    }
public:
    void solve() override {
//        find the largest prime factor of n
        long long n = 600851475143;
        long long res = 1, p = 1;

//        If the number is even, we can divide it by 2 right away
        if (n % 2 == 0) {
            n /= 2;
            res = 2;
        }

//        Maybe we are lucky and we've got a prime number
        if (check_if_prime(n)) {
            cout << n;
            return;
        }

//        Going through its prime factors
        for (p = 3; p <= n; p+=2) {
            if (n % p == 0) {
                if (check_if_prime(p)) {
                    res = n;
                }
                n/=p;
                p-=2;
            }
        }

        cout << res;
    }

    std::string get_problem_name() override {
        return std::string("Largest prime factor");
    }
};


#endif //PROGRAMMINGCHALLENGESC_PROBLEM3_H
