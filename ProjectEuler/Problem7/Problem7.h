#include "../Problem.h"

#include <iostream>

using namespace std;

class Problem7: public Problem {
    bool check_if_prime(long long number) {
        long long divisor;

        if (number % 2 == 0)
            return false;

        for (divisor = 3; divisor <= sqrt(number); divisor+=2)
            if (number % divisor == 0)
                return false;

        return true;
    }

    long long find_nth_prime(int nth_limit) {
        long long n;
        int nth;
        for (n = 6, nth = 2; nth < nth_limit; n += 6) {
            if (check_if_prime(n - 1)) {
                nth++;
                if (nth == nth_limit) {
                    return n - 1;
                }
            }

            if (check_if_prime(n + 1)) {
                nth++;
                if (nth == nth_limit) {
                    return n + 1;
                }
            }
        }

        return -1;
    }
public:
    void solve() override {
        cout << find_nth_prime(10001);
    }

    std::string get_problem_name() override {
        return std::string("10001st prime");
    }
};

