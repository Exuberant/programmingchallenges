#include "../Problem.h"

#include <iostream>
#include <vector>

using namespace std;

class Problem16: public Problem {
    //Computing the sum of digits of 2^power
    long power2_digit_sum(int power) {
        vector<int> digits;
        digits.push_back(1);

        int value;
        for (int p = 1; p <= power; ++p) {
            value = 0;
            for (int digit = static_cast<int>(digits.size()) - 1; digit >= 0; --digit) {
                value += 2 * digits[digit];
                digits[digit] = value % 10;
                value /= 10;
            }
            if (value != 0) {
                digits.insert(digits.begin(), value);
            }
        }

        long sum = 0;
        for (auto digit : digits) {
            sum += digit;
        }

        return sum;
    }
public:
    void solve() override {
        cout << power2_digit_sum(1000);
    }

    std::string get_problem_name() override {
        return std::string("Power digit sum");
    }
};
