#include "../Problem.h"

#include <iostream>

using namespace std;

class Problem9: public Problem {
    // a+b+c=1000 and (a,b,c) is a pythagorean triplet
    long special_pythagorean_triplet() {
        for (int a = 1; a < 1000; a++) {
            for (int b = a+1; b < 1000; b++) {
                int c = 1000 - a - b;
                if (a+b+c==1000 && a*a + b*b == c*c)
                    return a*b*c;
            }
        }

        return -1;
    }
public:
    void solve() override {
        cout << special_pythagorean_triplet();
    }

    std::string get_problem_name() override {
        return std::string("Special Pythagorean triplet");
    }
};
