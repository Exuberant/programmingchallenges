#include "../Problem.h"

#include <iostream>
#include <iomanip>

using namespace std;

class Problem6: public Problem {
public:
    void solve() override {
        int n = 100;

        cout << fixed << setprecision(0) << pow(n*(n+1)/2, 2) - (n*(n+1)*(2*n+1)/6);
    }

    std::string get_problem_name() override {
        return std::string("Sum square difference");
    }
};

